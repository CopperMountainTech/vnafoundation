#pragma once

#include "CalibrationRange.h"

#include <MultiportVector.h>

namespace Planar {
namespace VNA {

class VNAFOUNDATIONSHARED_EXPORT RcvCalibrationRange : public BaseCalibrationRange
{

public:
    RcvCalibrationRange(size_t npoints, Unit startOrCenter, Unit stopOrBand, size_t referencePort = 0);
    RcvCalibrationRange(const FrequencyRange& range, size_t referencePort = 0);
    RcvCalibrationRange(CalibrationRange* range); ///< Origin constructor with terms reference

    ///Planar::Dsp::Complex64 T(size_t port, size_t index) const; ///< indexed
    Planar::Dsp::Complex64 T(size_t referencePort, size_t testPort, size_t index) const;
    Planar::Dsp::Complex64 R(size_t index) const; ///< indexed
    Planar::Dsp::Complex64 R(size_t port, size_t index) const;

    // Adapt Receivers Calibarion Range to the another Frequency Range
    RcvCalibrationRange* adapt(const FrequencyRange& frequencyRange) override;
protected:
    RcvCalibrationRange(const FrequencyRange& range,
                        RcvCalibrationRange* origin); ///< Adaptation constructor

    void resize(size_t newsize, Unit startFrequency, Unit stopFrequency) override;
    void resize(size_t newsize) override;

    void clear() override;

    ///Planar::Dsp::Complex64 T(double testPortFreq, double referencePortFreq) const; ///< interpolated (test port freq not equal ref port freq : FOM mode)
    ///Planar::Dsp::Complex64 T(double freq) const; ///< interpolated (test port equal ref port)
    Planar::Dsp::Complex64 T(size_t referencePort, size_t testPort, Unit frequency) const;
    virtual Planar::Dsp::Complex64 R(Unit frequency) const;
    virtual Planar::Dsp::Complex64 R(size_t port, Unit frequency) const;

    void setT(size_t referencePort, size_t testPort, size_t index, Planar::Dsp::Complex64 value);
    void setR(size_t port, size_t index, Planar::Dsp::Complex64 value);

    bool isOrigin();
    RcvCalibrationRange* origin();
    void setOrigin(RcvCalibrationRange* origin);

private:
    RcvCalibrationRange*     _origin;   ///< origin receiver calibration range
    const CalibrationRange*  _terms;    ///< terms calibration range

    MultiportVector<Planar::Dsp::Complex64> _refReceiver;
    MultiportVector2D<Planar::Dsp::Complex64>    _testReceivers;
};


} // namespace VNA
} // namespase Planar
