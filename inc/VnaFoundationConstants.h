#pragma once

#include <Units.h>

#include "VnaFoundationGlobal.h"

namespace Planar {
//VNAFOUNDATIONSHARED_EXPORT Q_NAMESPACE
namespace VNA {
VNAFOUNDATIONSHARED_EXPORT Q_NAMESPACE

extern const  size_t BadItemIndex;

#define ARRAYLENGTH(ar) (sizeof(ar)/sizeof(ar[0]))

const size_t PORT_NUMBER = 2;
const size_t MIN_POINTS  =
    2;         // Мин. число точек в частотном диапазоне


enum class ReferenceSourceType { rsInt, rsExt };
Q_ENUM_NS(ReferenceSourceType);


enum class TriggerSourceType
{
    tsInt = 0,
    tsExt = 1,
    tsBus = 2
};
Q_ENUM_NS(TriggerSourceType)



enum class CorrectionType
{
    ctNone = 0,
    ctRT = 1,            // Response Thru
    ctRO = 2,            // Response Open
    ctRS = 3,            // Response Short
    ctF1 = 4,            // 1-port
    ctOP = 5             // 1-Path 2-Ports
};


enum class TriggerType
{
    tmHold,
    tmSingle,
    tmCont
};
Q_ENUM_NS(TriggerType)

QDebug operator<<(QDebug dbg, TriggerType enumValue);

const int TriggerTypeCount = static_cast<int>(TriggerType::tmCont) + 1;


enum class TriggerEventType
{
    teDisable = 0,
    teOnSweep = 1,
    teOnPoint = 2
};
Q_ENUM_NS(TriggerEventType)


enum class TriggerPolarityType
{
    tpNegativeEdge = 0,
    tpPositiveEdge = 1
};
Q_ENUM_NS(TriggerPolarityType)


enum class TriggerPositionType
{
    tpBeforeSampl = 0,
    tpBeforeSetup = 1,
};
Q_ENUM_NS(TriggerPositionType)


enum class TriggerOutPositionType
{
    tpBeforeSampl = 1,
    tpBeforeSetup = 0,
    tpAfterSampl = 2,
    tpExtTriggerReady = 3,
    tpEndOfScan = 4,
    tpFullScan = 5

};
Q_ENUM_NS(TriggerOutPositionType)


enum TriggerLevelType   // уровень/фронт:
{
    tlHigh = 0,   // высокий уровень;
    tlLow  = 1,   // низкий уровень;
    tlRise = 2,   // возрастающий фронт;
    tlFall = 3    // убывающий фронт;
};

enum FrontType
{
    ftRise = 0,   // возрастающий фронт;
    ftFall = 1    // убывающий фронт;
};

enum SourceTrigger : char
{
    stIntegrated = 0x00,
    stExternal0  = 0x01,
    stExternal1  = 0x02,
    stExternal2  = 0x03,
    stExternal3  = 0x04,
    stModulator  = 0x05
};

enum TriggerCapture   // захват триггера до готовности прибора (только по фронту)
{
    tcOff = 0,
    tcOn  = 1
};


enum class ScannerStateType
{
    ssHold,
    ssWaitTrigger,
    ssMeas
};
Q_ENUM_NS(ScannerStateType)


VNAFOUNDATIONSHARED_EXPORT QDebug operator<<(QDebug dbg, ScannerStateType enumValue);

const int ScannerTypeCount = static_cast<int>(ScannerStateType::ssMeas) + 1;


enum CalibrationStandardType
{
    csThru = 0,
    csShort = 1,
    csOpen = 2,
    csLoad = 3,
    csOpen2 = 4,
    csLoad2 = 5,
    csNone = 0xFFFFFFFF
};


// выбор фильтра (C3220)
enum ScannerFilterType
{
    sfNone      = 0, // без фильтра (режим осциллографа)
    sfRectangle = 1, // прямоугольное окно
    sfSinus     = 2, // синус – окно
    sfAveraging = 3
};


enum MeasurementMode
{
    mmVNA              = 0,
    mmOscill           = 1,
    mmPulseProfile     = 2,
    mmPulseToPulse     = 3,
    mmPointInImpulse   = 4,
    mmAveragingOfPulse = 5,
    mmNone             = 0xFFFFFFFF
};

enum ImpulseMode
{
    imProfile        = 0,
    imPulseToPulse   = 1,
    imPointInImpulse = 2,
    imAveraging      = 3,
    imNone           = 0xFFFFFFFF
};


extern const  Planar::Unit VNAFOUNDATIONSHARED_EXPORT DefaultFrequency;
extern const  Planar::Unit VNAFOUNDATIONSHARED_EXPORT DefaultBandIF;
extern const  Planar::Unit VNAFOUNDATIONSHARED_EXPORT DefaultPower;





} // namespace VNA
} // namespase Planar

