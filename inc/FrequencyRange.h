#pragma once

#include "VnaFoundationGlobal.h"
#include "VnaFoundationConstants.h"

namespace Planar {
namespace VNA {

struct  FrequencyPoint {
    FrequencyPoint(Unit freq) : frequency(freq) {}

    Unit frequency;
};


class  VNAFOUNDATIONSHARED_EXPORT FrequencyRange
{
public:
    FrequencyRange() {}
    FrequencyRange(size_t npoints, Unit value = DefaultFrequency);
    FrequencyRange(size_t npoints, Unit startOrCenter, Unit stopOrBand);
    FrequencyRange(const FrequencyRange& range);
    virtual ~FrequencyRange() = default;

    Unit frequency(size_t index) const
    {
        return _frequency[index];
    }
    void setFrequency(size_t index, Unit value)
    {
        _frequency[index] = value;
    }
    double frequencyVna(size_t index) const
    {
        return _frequency[index].valueVna();
    }

    FrequencyPoint frequencyPoint(size_t index) const
    {
        return FrequencyPoint(frequency(index));
    }

    void setPoint(size_t index, Unit frequency)
    {
        setFrequency(index, frequency);
    }
    void setPoint(size_t index, FrequencyPoint& point)
    {
        setPoint(index, point.frequency);
    }

    virtual void addPoint(Unit frequency)
    {
        _frequency.push_back(frequency);
    }
    virtual void addPoint(FrequencyPoint& point)
    {
        _frequency.push_back(point.frequency);
    }

    size_t size() const
    {
        return _frequency.size();
    }

    virtual void resize(size_t newsize)
    {
        _frequency.resize(newsize, DefaultFrequency);
    }

    virtual void resize(size_t newsize, Unit startFrequency, Unit stopFrequency);

    virtual void reservePoints(size_t npoints)
    {
        _frequency.reserve(npoints);
    }

    virtual void clear()
    {
        _frequency.clear();
    }

    void resizeBand(size_t newsize, Unit centerFrequency, Unit bandFrequency)
    {
        auto half = bandFrequency / 2.;
        resize(newsize, centerFrequency - half, centerFrequency + half);
    }


protected:
    std::vector<Unit> _frequency;

    ///
    /// \brief Поиск индекса i по заданной частоте F, где F принадлежит [Fi, Fi+1),
    ///        Диапазон возвращаемых значений i = [0, Count - 2].
    ///        Если F < Fmin, то возращается 0, если F > Fmax, то возвращается Count - 2
    /// \param Частота
    /// \return Индекс
    ///
    size_t index(double freq) const;
    size_t index(double freq, double& factor) const;

    double interpolationFactor(double f, size_t& i0, size_t& i1) const;
private:
    void resizeEx(size_t newsize, Unit startFrequency, Unit stopFrequency);
    size_t subIndex(double F, size_t Beg, size_t End) const;
};

} // namespace VNA
} // namespase Planar

