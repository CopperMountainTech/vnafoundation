#pragma once

#include <ComplexVector.h>

#include "VnaFoundationGlobal.h"
#include "VnaFoundationConstants.h"
#include "MeasuringResult.h"
#include "FrequencyRange.h"

namespace Planar {
namespace VNA {

struct VNAFOUNDATIONSHARED_EXPORT ScanPointBase : public FrequencyPoint {
    ScanPointBase(Unit frequencyValue, Planar::Unit powerValue, Planar::Unit ifBandwidth)
        : FrequencyPoint(frequencyValue), power(powerValue), IFBW(ifBandwidth)
    {}

    Planar::Unit power;
    Planar::Unit IFBW;
};


class VNAFOUNDATIONSHARED_EXPORT ScanRangeBase : public QObject
{
    Q_OBJECT

public:
    ScanRangeBase(QObject* parent = nullptr);
    ScanRangeBase(size_t npoints, QObject* parent = nullptr);
    //ScanRangeBase(const ScanRangeBase& range);

    Unit frequency(size_t index) const
    {
        return _frequency[index];
    }

    void setFrequency(size_t index, Unit value)
    {
        _frequency[index] = value;
    }

    Unit IFBW(size_t index) const
    {
        return _bandIF[index];
    }

    void setIFBW(size_t index, Unit value)
    {
        _bandIF[index] = value;
    }

//    double IFBW(size_t index) const
//    {
//        return _bandIF[index].valueVna();
//    }

    Unit power(size_t index) const
    {
        return _power[index];
    }

    void setPower(size_t index, Unit value)
    {
        _power[index] = value;
    }

//    double Power(size_t index) const
//    {
//        return _power[index].valueVna();
//    }

    ScanPointBase getScanPoint(size_t index) const
    {
        return ScanPointBase(frequency(index), power(index), IFBW(index));
    }

    void addPoint(Unit frequency, Unit power, Unit ifBandwidth);

    void addPoint(ScanPointBase point)
    {
        addPoint(point.frequency, point.power, point.IFBW);
    }

    int size() const
    {
        return (int)_frequency.size();
    }

    virtual void resize(int newsize);

    void reservePoints(size_t npoints)
    {
        _frequency.reserve(npoints);
        _bandIF.reserve(npoints);
        _power.reserve(npoints);
    }

    void clear()
    {
        _frequency.clear();
        _bandIF.clear();
        _power.clear();
    }

    virtual void setPoint(size_t index, Unit frequency, Unit power, Unit ifBandwidth);

    void setPoint(size_t index, ScanPointBase& point)
    {
        setPoint(index, point.frequency, point.power, point.IFBW);
    }

    MeasurementMode measurementMode() const
    {
        return _measMode;
    }

    ScannerFilterType filterType() const
    {
        return _filterType;
    }

    void copyData(const ScanRangeBase& range);

    int buildId() const
    {
        return _buildId;
    }

    bool isEnabled()
    {
        return _isEnabled;
    }
    void setEnabled(bool value)
    {
        _isEnabled = value;
    }

    // список индексов портов, по которым надо делать росчерк
    // индексы от пронумерованы от 0
    QList<int>& activePortList()
    {
        return _activePortList;
    }

    bool isReverseScan() const
    {
        return _isReverseScan;
    }
    void setReverseScan(bool value)
    {
        _isReverseScan = value;
    }

signals:
    void rebuildStarted();
    void rebuildFinished(int buildId);
    void activePortListChanged();

public slots:
    virtual void onMeasuringResultReady(MeasuringResult* result)
    {
        Q_UNUSED(result)
    }

    // обновляет buildId и генерит сигнал rebuildFinished
    void onRebuildFinished();

protected:
    //bool _FOMode;
    void setBuildId(int id)
    {
        _buildId = id;
    }

    QList<int> _activePortList;

    MeasurementMode   _measMode;
    ScannerFilterType _filterType;

    std::vector<Unit> _bandIF;
    std::vector<Unit> _power;
    std::vector<Unit> _frequency;

    int  _buildId;
    bool _isEnabled = true;
    bool _isReverseScan;

    //ItemPointerCollection<ScanPort> _ports;
};

/*
using DoubleCollection = std::vector<double>;
using SubRange = std::pair<int, int>;


struct FrequencySegment   // только для росчерка по частоте
{
    double BegFreq;
    double EndFreq;
    int Points;
    int IFBW;
    double Delay;
    double OutPower;
};

struct BaseSetting
{
    SweepType SweepMode;
    int    Points;
    double BegFreq;
    double EndFreq;
    double MinPower;
    double MaxPower;
    double FixedFreq;
    //
    bool   FOMState;
    double FOMOffset[2];
    int    FOMMultiplier[2];
    int    FOMDivider[2];
    bool   FOMAdjust;
    int    FOMAdjPort;
    double FOMAdjValue[2];
    //
    int    CommonIFBW;
    double CommonDelay;
    double OutPower;
    double PowerSlope;
    bool   SegmentListIFBW;
    bool   SegmentListPower;
    bool   SegmentListDelay;
    int    SegmentCount;
};

struct ScanRangeParameters
{
    int    Points;
    SweepType SweepMode;
    int    CommonIFBW;

    double BegFreq;
    double EndFreq;
    double CommonDelay;
    double OutPower;
    double PowerSlope;
    double MinPower;
    double MaxPower;
    double FixedFreq;

    bool   FOMState;
    double FOMOffset[2];
    int    FOMMultiplier[2];
    int    FOMDivider[2];
    bool   FOMAdjust;
    int    FOMAdjPort;
    double FOMAdjValue[2];
    bool   FOMAutoAdjust;
    FOMAdjustPeriodLength FOMAdjPeriod;

    bool   SegmentCenterPriority;
    bool   SegmentListIFBW;
    bool   SegmentListPower;
    bool   SegmentListDelay;

    bool   ZConvEnabled;
    double ZConvValue[2];
    bool   EmbEnabled[2];
    QString EmbFile[2];
    bool   DeembEnabled[2];
    QString DeembFile[2];

    bool   AverageEnabled;
    int    AverageFactor;
};


struct FailBound
{
    int  SegmentIndex;
    bool BegFail;
    bool EndFail;
};
*/

} // namespace VNA
} // namespase Planar
