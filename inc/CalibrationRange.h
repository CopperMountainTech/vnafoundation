#pragma once

#include <QString>
#include <AbstractSampleVector.h>
#include <Complex.h>

#include "VnaFoundationGlobal.h"
#include "FrequencyRange.h"

namespace Planar {
namespace VNA {

class VNAFOUNDATIONSHARED_EXPORT Correction
{

    ///
    /// \brief The Correction Type enum
    ///         RT - Response Though
    ///         RO - Response Open
    ///         RS - Response Short
    ///         F1 - Full 1-port
    ///         OP - 1-Path 2-Ports

    enum Type { None = 0, RT = 1, RO = 2, RS = 4, F1 = RO | RS, OP = F1 | RT };

    static QString toString(Correction::Type& type)
    {
        switch (type) {
        case None:
            return QStringLiteral("None");
        case RT:
            return QStringLiteral("RT");
        case RO:
            return QStringLiteral("RO");
        case RS:
            return QStringLiteral("RS");
        case OP:
            return QStringLiteral("OP");
        case F1:
            return QStringLiteral("F1");
        default:
            qWarning("Correction::ToString");
            return QStringLiteral("None");
        }
    }
};


struct  VNAFOUNDATIONSHARED_EXPORT CalibrationPoint : public FrequencyPoint {
    CalibrationPoint(Unit frequency, CorrectionType corrType = CorrectionType::ctNone)
        : FrequencyPoint(frequency), type(corrType) {}

    // copy
    CalibrationPoint(const CalibrationPoint& point)
        : FrequencyPoint(point), type(point.type) {}

    CorrectionType type; ///< correction type
};


///
/// \brief The ReflectionPoint struct
///
struct VNAFOUNDATIONSHARED_EXPORT ReflectionPoint : public CalibrationPoint {
public:
    ReflectionPoint(Unit frequency,
                    CorrectionType type = CorrectionType::ctNone,
                    Planar::Dsp::Complex64 er = Planar::Dsp::complexZero64(),
                    Planar::Dsp::Complex64 ed = Planar::Dsp::complexZero64(),
                    Planar::Dsp::Complex64 es = Planar::Dsp::complexZero64())
        : CalibrationPoint(frequency, type), Er(er), Ed(ed), Es(es) {}

    // copy
    ReflectionPoint(const ReflectionPoint& other)
        : CalibrationPoint(other), Er(other.Er), Ed(other.Ed), Es(other.Es) {}

    Planar::Dsp::Complex64 Er; ///< reflection error
    Planar::Dsp::Complex64 Ed; ///< directivity error
    Planar::Dsp::Complex64 Es; ///< source match error
};


///
/// \brief The TransitionPoint struct
///
struct  VNAFOUNDATIONSHARED_EXPORT TransitionPoint : public CalibrationPoint {
public:
    TransitionPoint(Unit frequency, CorrectionType type = CorrectionType::ctNone,
                    Planar::Dsp::Complex64 et = Planar::Dsp::complexZero64(),
                    Planar::Dsp::Complex64 ex = Planar::Dsp::complexZero64(),
                    Planar::Dsp::Complex64 el = Planar::Dsp::complexZero64() )
        : CalibrationPoint(frequency, type), Et(et), Ex(ex), El(el) {}

    // copy
    TransitionPoint(const TransitionPoint& other)
        : CalibrationPoint(other), Et(other.Et), Ex(other.Ex), El(other.El) {}

    Planar::Dsp::Complex64 Et; ///< transition error

    Planar::Dsp::Complex64 Ex; ///< directivity error
    Planar::Dsp::Complex64 El; ///< load match error
};

///
/// \brief The BaseCalibrationRange class
///
class  VNAFOUNDATIONSHARED_EXPORT BaseCalibrationRange : public FrequencyRange
{
public:
    bool isValid() const;

    void enable();
    void disable();
    bool isEnabled() const;

    size_t referencePort() const;
protected:
    BaseCalibrationRange(size_t npoints, Unit startOrCenter, Unit stopOrBand, size_t referencePort = 0);
    BaseCalibrationRange(const FrequencyRange& range, size_t referencePort = 0);

    virtual ~BaseCalibrationRange() = default;
    virtual BaseCalibrationRange* adapt(const FrequencyRange& range) = 0;

    void validate();
    void setReferencePort(size_t port);
private:
    size_t _refPort;
    bool _valid;
    bool _enabled;
};

inline void BaseCalibrationRange::enable()
{
    _enabled = true;
}

inline void BaseCalibrationRange::disable()
{
    _enabled = false;
}

inline bool BaseCalibrationRange::isEnabled() const
{
    return _enabled;
}


///
/// \brief The CalibrationRange class
///
class  VNAFOUNDATIONSHARED_EXPORT CalibrationRange : public BaseCalibrationRange
{
public:
    CalibrationRange(size_t npoints, Unit startOrCenter, Unit stopOrBand, size_t referencePort = 0);
    CalibrationRange(const FrequencyRange& range, size_t referencePort = 0);
    virtual ~CalibrationRange() = default;

    CalibrationRange* adapt(const FrequencyRange& frequencyRange) override;

    CorrectionType correctionMode(size_t sourcePort, size_t testPort ) const;
    CorrectionType correctionMode(size_t port) const;
    CorrectionType correctionMode() const;
    QString correctionName(size_t port) const;

    /// indexed terms
    Planar::Dsp::Complex64 Er(size_t port, size_t index) const;
    Planar::Dsp::Complex64 Ed(size_t port, size_t index) const;
    Planar::Dsp::Complex64 Es(size_t port, size_t index) const;

    Planar::Dsp::Complex64 Et(size_t referencePort, size_t testPort, size_t index) const;
    Planar::Dsp::Complex64 Ex(size_t referencePort, size_t testPort, size_t index) const;
    Planar::Dsp::Complex64 El(size_t referencePort, size_t testPort, size_t index) const;

    /// interpolated terms
    Planar::Dsp::Complex64 Er(size_t port, double freq) const;
    Planar::Dsp::Complex64 Et(size_t referencePort, size_t testPort, double freq) const;

    /// obsolete interpolated terms
    void computeEtTerm(size_t referencePort, size_t testPort, double f,
                       Planar::Dsp::Complex64& Et) const;
    void computeF1Terms(size_t port, double f, Planar::Dsp::Complex64& Er, Planar::Dsp::Complex64& Ed,
                        Planar::Dsp::Complex64& Es) const; ///< obsolete

    void computeInterpolatedTerms(size_t port, double f, Planar::Dsp::Complex64& Er,
                                  Planar::Dsp::Complex64& Ed, Planar::Dsp::Complex64& Es) const;
    void computeInterpolatedTerms(size_t referencePort, size_t testPort, double f,
                                  Planar::Dsp::Complex64& _Et,
                                  Planar::Dsp::Complex64& _Ex, Planar::Dsp::Complex64& _El) const;

    /// Unitized interpolated terms
    void computeEtTerm(size_t referencePort, size_t testPort, Unit f, Planar::Dsp::Complex64& Et) const
    {
        computeEtTerm(referencePort, testPort, f.valueVna(), Et);
    }
    void computeF1Terms(size_t port, Unit f, Planar::Dsp::Complex64& Er, Planar::Dsp::Complex64& Ed,
                        Planar::Dsp::Complex64& Es) const
    {
        computeF1Terms(port, f.valueVna(), Er, Ed, Es);
    }

    void computeInterpolatedTerms(size_t port, Unit f, Planar::Dsp::Complex64& _Er,
                                  Planar::Dsp::Complex64& _Ed, Planar::Dsp::Complex64& _Es) const
    {
        computeInterpolatedTerms(port, f.valueVna(), _Er, _Ed, _Es);
    }
    void computeInterpolatedTerms(size_t referencePort, size_t testPort, Unit f,
                                  Planar::Dsp::Complex64& _Et,
                                  Planar::Dsp::Complex64& _Ex,
                                  Planar::Dsp::Complex64& _El) const
    {
        computeInterpolatedTerms(referencePort, testPort, f.valueVna(), _Et, _Ex, _El);
    }

    ReflectionPoint reflectionPoint(size_t port, size_t index) const;
    TransitionPoint transitionPoint(size_t referencePort, size_t testPort, size_t index) const;

protected:
    CalibrationRange(const FrequencyRange& range, CalibrationRange* origin);

    void setCorrectionMode(CorrectionType mode);
    void setCorrectionMode(size_t port, CorrectionType mode);
    void setCorrectionMode(size_t referencePort, size_t testPort, CorrectionType mode);

    void setEr(size_t port, size_t index, Planar::Dsp::Complex64 value);
    void setEd(size_t port, size_t index, Planar::Dsp::Complex64 value);
    void setEs(size_t port, size_t index, Planar::Dsp::Complex64 value);

    void setEt(size_t referencePort, size_t testPort, size_t index, Planar::Dsp::Complex64 value);
    void setEx(size_t referencePort, size_t testPort, size_t index, Planar::Dsp::Complex64 value);
    void setEl(size_t referencePort, size_t testPort, size_t index, Planar::Dsp::Complex64 value);

    void addPoint(Unit freq) override;

    void addPoint(FrequencyPoint& point) override
    {
        addPoint(point.frequency);
    }

    void setReflectionPoint(size_t port, size_t index, const ReflectionPoint& rPoint);

    void setTransitionPoint(size_t referencePort, size_t testPort, size_t index,
                            const TransitionPoint& tPoint);

    void setPoint(size_t index,
                  std::initializer_list<std::pair<std::pair<size_t, size_t>,
                  const CalibrationPoint&>> points);

    void resize(size_t newsize) override;

    bool isOrigin()
    {
        return _origin == nullptr;
    }

    CalibrationRange* origin()
    {
        return _origin;
    }

    void setOrigin(CalibrationRange* origin)
    {
        _origin = origin;
    }

private:
    ///
    /// \brief The ErrorTerms base class
    ///
    class ErrorTerms
    {
    protected:
        ErrorTerms();
        void setType(CorrectionType type)
        {
            _correctionType = type;
        }

    public:
        CorrectionType type()
        {
            return _correctionType;
        }
        virtual ~ErrorTerms() = default;
        virtual size_t size() = 0;
        virtual void resize(size_t size) = 0;
//        virtual void AddPoint(ErrorTerms& point) = 0;
//        virtual void SetPoint(size_t index, ErrorTerms& point) = 0;
    private:
        CorrectionType _correctionType;
        friend class CalibrationRange;
    };

    ///
    /// \brief The ReflectionErrorTerms class
    ///
    class ReflectionErrorTerms : public ErrorTerms
    {
    public:
        ReflectionErrorTerms(size_t size);
        virtual ~ReflectionErrorTerms() = default;

        size_t size() override;
        void resize(size_t size) override;
        void addPoint(const ReflectionPoint& point);
        void setPoint(size_t index, const ReflectionPoint& point);

        std::vector<Planar::Dsp::Complex64> Er;
        std::vector<Planar::Dsp::Complex64> Ed;
        std::vector<Planar::Dsp::Complex64> Es;
    };

    ///
    /// \brief The TransitionErrorTerms class
    ///
    class TransitionErrorTerms : public ErrorTerms
    {
    public:
        TransitionErrorTerms(size_t size);
        virtual ~TransitionErrorTerms() = default;

        size_t size() override;
        void resize(size_t size) override;
        void addPoint(const TransitionPoint& point);
        void setPoint(size_t index, const TransitionPoint& point);

        std::vector<Planar::Dsp::Complex64> Et;
        std::vector<Planar::Dsp::Complex64> Ex;
        std::vector<Planar::Dsp::Complex64> El;
    };

    CorrectionType _correctionMode;
    std::vector< std::vector<ErrorTerms*> > _errorTerms;

    CalibrationRange* _origin;

    static const Planar::Dsp::Complex64 DefaultEr;
    static const Planar::Dsp::Complex64 DefaultEd;
    static const Planar::Dsp::Complex64 DefaultEs;

    static const Planar::Dsp::Complex64 DefaultEt;
    static const Planar::Dsp::Complex64 DefaultEx;
    static const Planar::Dsp::Complex64 DefaultEl;
};

inline void CalibrationRange::setCorrectionMode(CorrectionType mode)
{
    _correctionMode = mode;
}

} // namespace VNA
} // namespase Planar
