#pragma once

#include <QVector>
#include <complex>
//#include <QSemaphore>
#include <atomic>

#include <ComplexVector.h>
#include <MultiportVector.h>

#include "VnaFoundationGlobal.h"

namespace Planar {
namespace VNA {

class MeasuringResult
{
public:
    // резервируем место на максимальное кол-во точек в одном блоке данных
    MeasuringResult(int numberOfPorts = 2, int size = 1024,
                    bool enSense = false)
        : dataT(numberOfPorts, 0), dataR(numberOfPorts, 0), sensT(numberOfPorts, 0),
          sensR(numberOfPorts, 0)
    {
        dataT.reserve(size * numberOfPorts);
        sensT.reserve(size * numberOfPorts);
        sensT.reserve(size * numberOfPorts);
        if (enSense) sensR.reserve(size * numberOfPorts);
        // activePort.reserve(size * numberOfPorts);
        _activePort = 0;
        _startIndex = 0;
        _rangeId = 0;
        isReleased = true;
    }

    ~MeasuringResult()
    {
        clear();
    }

    size_t startIndex()
    {
        return _startIndex;    // Смещение в Range
    }

    void setStartIndex(size_t index)
    {
        _startIndex = index;
    }

    void clear()                       // Сброс размера данных в 0
    {
        dataT.clear();
        dataR.clear();
        sensT.clear();
        sensR.clear();
        //   activePort.clear();
        _startIndex = 0;
    }

    void appendPoint(std::vector<Planar::Dsp::Complex64> tp, Planar::Dsp::Complex64 rp )
    {
        Q_ASSERT(tp.size() == dataT.size());
        //   activePort.push_back(0);
        dataT.pushData(tp);
        dataR.pushData(rp);
    }

    void appendPoint(std::vector<Planar::Dsp::Complex64> tp, std::vector<Planar::Dsp::Complex64> rp)
    {
        Q_ASSERT(tp.size() == dataT.size());
        //   activePort.push_back(0);
        dataT.pushData(tp);
        dataR.pushData(rp);
    }

    void appendPoint(unsigned char port, std::vector<Planar::Dsp::Complex64> tp,
                     std::vector<Planar::Dsp::Complex64> rp)
    {
        Q_ASSERT(tp.size() == dataT.size());
        //   activePort.push_back(port);
        _activePort = port;     // не эффективно. Порт не меняется!
        dataT.pushData(tp);
        dataR.pushData(rp);
    }

    void appendPoint(std::vector<Planar::Dsp::Complex64> tp, Planar::Dsp::Complex64 rp,
                     std::vector<unsigned char> tSense, unsigned char rSense)
    {
        // activePort.push_back(0);
        appendPoint(tp, rp);
        sensT.pushData(tSense);
        sensR.pushData(rSense);
    }

    size_t size()
    {
        return dataR[0].size();         // кол-во точек
    }

    // данные тестовых приёмников [port][points]
    MultiportVector<Planar::Dsp::Complex64> dataT;

    // данные опорного приёмника стимулирующего порта [port][points]
    MultiportVector<Planar::Dsp::Complex64> dataR;

    // чувствительности тестовых приёмников [port][points]
    MultiportVector<unsigned char> sensT;

    // чувствительность опорного приёмника стимулирующего порта [port][points]
    MultiportVector<unsigned char> sensR;

    // номер активного порта для измеренной точки
    //QVector<unsigned char> activePort;
    inline int activePort()
    {
        return _activePort;
    }

    inline void setActivePort(int port)
    {
        _activePort = port;
    }

    // идентификатор range. изменяется при изменении range
    int rangeId()
    {
        return _rangeId;
    }

    // идентификатор range
    void setRangeId(int id)
    {
        _rangeId = id;
    }

    // сигнализация об окончании обработки данных
    void release()
    {
        isReleased = true;
    }

    std::atomic_bool isReleased;

private:
    size_t _startIndex;
    int _rangeId;
    int _activePort;
};


} // namespace VNA
} // namespase Planar
