#pragma once

#include <QtCore/qglobal.h>

#include <Units.h>

#if defined(VNAFOUNDATION_LIBRARY)
#define VNAFOUNDATIONSHARED_EXPORT Q_DECL_EXPORT
#else
#define VNAFOUNDATIONSHARED_EXPORT Q_DECL_IMPORT
#endif

