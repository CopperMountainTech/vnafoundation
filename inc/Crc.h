#pragma once

#include "VnaFoundationGlobal.h"

namespace Planar {
namespace VNA {

//VNAFOUNDATIONSHARED_EXPORT unsigned char swingByte(unsigned char value);
VNAFOUNDATIONSHARED_EXPORT unsigned long CRC32(unsigned long crc, const unsigned char* buf,
                                               unsigned long len);
VNAFOUNDATIONSHARED_EXPORT unsigned long ICRC32(unsigned long crc, const unsigned char* buf,
                                                unsigned long len);
//VNAFOUNDATIONSHARED_EXPORT bool fileCRC32(char* filename, quint32& crc);

} // namespace VNA
} // namespase Planar


